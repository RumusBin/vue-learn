import Vue from 'vue'
import App from './Components/App.vue'
import Message from './Components/Message'
import QuestionScreen from './Components/QuestionScreen'
import ResultScreen from './Components/ResultScreen'
import StartScreen from './Components/StartScreen'

Vue.component('Message', Message);
Vue.component('ResultScreen', ResultScreen);
Vue.component('QuestionScreen', QuestionScreen);
Vue.component('StartScreen', StartScreen);

new Vue({
  el: '#app',

  render: h => h(App)
})
